# INF222-V24, Obligatory 1

## Submission meta-info

* Student's name: _your name_
* Student's UiB user handle: _abc123_
* Last digit: 3
* Concrete syntax choice (based on the last digit):
    * Java-like (curly braces)
    * English-like syntax for assignment statements

## Abstract syntax in Haskell

```haskell
-- your Haskell code here
```

## Abstract syntax in Zephyr ASDL
```
// specification here
```

## Concrete syntax: grammar

```
// grammar here
```

## Sample program

```
// code here
```

## Reflections on language design, orthogonality

(your answers here)


## Other comments (if any)

(possibly, any other comments you'd like to make)
